from rest_framework import serializers

from user.models import UserModel


class UserAuthInSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=150)
    password = serializers.CharField(max_length=128)


class UserOutSerializer(serializers.ModelSerializer):

    class Meta:

        model = UserModel
        fields = 'id', 'username', 'email', 'date_joined', 'first_name', 'last_name', 'balance'
