from datetime import datetime

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Sum
from django.utils.timezone import make_aware
from mimesis import Person, Datetime, Internet

from operation.models import OperationModel


class UserModel(AbstractUser):

    ip_address = models.GenericIPAddressField(default='127.0.0.1')
    last_login = models.DateTimeField(default=datetime.now)

    @classmethod
    def generate(cls) -> 'UserModel':
        p = Person()
        d = Datetime()
        i = Internet()

        username = p.username()
        password = p.password()
        email = p.email()
        first_name = p.first_name()
        last_name = p.last_name()
        date_joined = make_aware(d.datetime())
        ip_address = i.ip_v4()
        last_login = make_aware(d.datetime())

        user = UserModel.objects.create_user(
            username=username, password=password, email=email,
            first_name=first_name, last_name=last_name, date_joined=date_joined,
            ip_address=ip_address, last_login=last_login)
        user.raw_password = password

        return user

    @property
    def balance(self):
        """
        Returns user's balance based on it's income & outcome operations.
        """
        in_ops = self.operations.filter(state=OperationModel.State.SUCCESS, type=OperationModel.Type.INCOME)
        out_ops = self.operations.filter(state=OperationModel.State.SUCCESS, type=OperationModel.Type.OUTCOME)

        amount_in = in_ops.aggregate(amount_sum=Sum('amount'))
        amount_in = amount_in['amount_sum'] or 0
        amount_out = out_ops.aggregate(amount_sum=Sum('amount'))
        amount_out = amount_out['amount_sum'] or 0
        amount = amount_in - amount_out
        amount = round(amount / 100, 2)  # TODO: AssetModel.get_decimals(self.symbol)

        return amount
