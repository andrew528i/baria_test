import pytest

from operation.models import OperationModel


@pytest.mark.django_db
def test_user_balance(user):
    for _ in range(100):
        OperationModel.generate(user)

    balance = user.balance

    assert isinstance(balance, float)
