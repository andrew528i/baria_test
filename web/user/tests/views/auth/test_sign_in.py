import pytest
from rest_framework import status


@pytest.mark.django_db
def test_user_auth_sign_in(client, user):
    request = dict(username=user.username, password=user.raw_password)
    resp = client.post('/user/auth/sign_in', request)
    data = resp.json()
    resp_user = data['user']

    assert resp.status_code == status.HTTP_200_OK
    assert resp_user['id'] == user.id
    assert resp_user['username'] == user.username
    assert resp_user['balance'] == 0


@pytest.mark.django_db
def test_user_auth_sign_in_error(client, user):
    wrong_password = '1q2w3e4r5t6y'
    request = dict(username=user.username, password=wrong_password)
    resp = client.post('/user/auth/sign_in', request)

    assert resp.status_code == status.HTTP_403_FORBIDDEN

