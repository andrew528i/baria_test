from datetime import datetime

from django.contrib.auth import authenticate, login
from django.utils.timezone import make_aware
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from user.serializers import UserAuthInSerializer, UserOutSerializer


class WrongCredentials(APIException):

    status_code = status.HTTP_403_FORBIDDEN
    default_detail = 'Wrong credentials'
    default_code = 'wrong_credentials'


class AlreadyAuthenticated(APIException):

    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Already authenticated'
    default_code = 'already_authenticated'


class UserSignInView(APIView):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request: Request):
        serializer = UserAuthInSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if request.user.is_authenticated:
            raise AlreadyAuthenticated

        username = request.data['username']
        password = request.data['password']
        user = authenticate(username=username, password=password)

        if not user:
            raise WrongCredentials

        now = datetime.now()
        user.last_login = make_aware(now)
        user.ip_address = self.get_ip_address(request)
        user.save()

        login(request, user)

        return Response(dict(user=UserOutSerializer(user).data))

    def get_ip_address(self, request: Request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

        if x_forwarded_for:
            ip_address = x_forwarded_for.split(',')[-1].strip()
        else:
            ip_address = request.META.get('REMOTE_ADDR')

        return ip_address
