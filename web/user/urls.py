from django.urls import path

from user.views.auth.sign_in import UserSignInView

urlpatterns = [
    path('auth/sign_in', UserSignInView.as_view()),
]
