import pytest
from rest_framework import status

from user.models import UserModel


@pytest.fixture
def user():
    return UserModel.generate()


@pytest.fixture
def authorized_client(client, user):
    request = dict(username=user.username, password=user.raw_password)
    resp = client.post('/user/auth/sign_in', request)

    assert resp.status_code == status.HTTP_200_OK

    return client
