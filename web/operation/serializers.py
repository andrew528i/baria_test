from rest_framework import serializers

from operation.models import OperationModel


class OperationListInSerializer(serializers.Serializer):

    datetime_from = serializers.DateTimeField()
    datetime_to = serializers.DateTimeField()


class OperationStatsInSerializer(serializers.Serializer):

    datetime_from = serializers.DateTimeField()
    datetime_to = serializers.DateTimeField()


class OperationOutSerializer(serializers.ModelSerializer):

    class Meta:

        model = OperationModel
        fields = 'id', 'amount', 'symbol', 'type', 'state', 'created_at', 'updated_at'
