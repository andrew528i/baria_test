import random
import typing
from datetime import datetime, timedelta

from django.db import models
from django.utils.timezone import make_aware
from django.utils.translation import gettext_lazy as _
from mimesis import Business

if typing.TYPE_CHECKING:
    from user.models import UserModel


class OperationModel(models.Model):

    class Type(models.TextChoices):

        INCOME = 'IN', _('Income')
        OUTCOME = 'OUT', _('Outcome')

    class State(models.TextChoices):

        QUEUED = 'QUEUED', _('Queued')
        PROCESSING = 'PROCESSING', _('Processing')
        SUCCESS = 'SUCCESS', _('Success')
        ERROR = 'ERROR', _('Error')

    type = models.CharField(max_length=3, choices=Type.choices)
    state = models.CharField(max_length=10, choices=State.choices)
    amount = models.BigIntegerField()
    symbol = models.CharField(max_length=9)
    user = models.ForeignKey('user.UserModel', on_delete=models.CASCADE, related_name='operations')
    created_at = models.DateTimeField(default=datetime.now)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def generate(cls, user: 'UserModel') -> 'OperationModel':
        b = Business()
        type_ = random.choice([OperationModel.Type.INCOME, OperationModel.Type.OUTCOME])
        state = random.choice([
            OperationModel.State.QUEUED, OperationModel.State.PROCESSING,
            OperationModel.State.SUCCESS, OperationModel.State.ERROR])
        amount = random.choice(range(5, 500000, 10))
        symbol = b.currency_iso_code()
        now = datetime.now()
        days = random.choice(range(365))
        created_at = make_aware(now - timedelta(days=days))

        return OperationModel.objects.create(
            type=type_, state=state,
            amount=amount, symbol=symbol,
            user=user, created_at=created_at)
