from datetime import datetime, timedelta

import pytest
from django.utils.timezone import make_aware
from rest_framework import status

from operation.models import OperationModel


@pytest.mark.django_db
def test_operation_list(authorized_client, user):
    operation = OperationModel.generate(user)
    datetime_to = make_aware(datetime.now())
    datetime_from = datetime_to - timedelta(days=365)
    request = dict(datetime_from=datetime_from, datetime_to=datetime_to)
    resp = authorized_client.get('/operation/', request)
    data = resp.json()
    resp_operation = data['results'][0]

    assert resp.status_code == status.HTTP_200_OK
    assert resp_operation['id'] == operation.id


def test_operation_list_error(client):
    resp = client.get('/operation/')

    assert resp.status_code == status.HTTP_403_FORBIDDEN
