from datetime import datetime, timedelta

import pytest
from django.utils.timezone import make_aware
from rest_framework import status

from operation.models import OperationModel


@pytest.mark.django_db
def test_operation_stats(authorized_client, user):
    for _ in range(50):
        OperationModel.generate(user)

    datetime_to = make_aware(datetime.now())
    datetime_from = datetime_to - timedelta(days=365)
    request = dict(datetime_from=datetime_from, datetime_to=datetime_to)
    resp = authorized_client.get('/operation/stats', request)
    data = resp.json()

    # TODO: check logic of stat's fields generation
    assert resp.status_code == status.HTTP_200_OK
    assert 'income_count' in data
    assert 'income_amount' in data
    assert 'outcome_count' in data
    assert 'outcome_amount' in data
