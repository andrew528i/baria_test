from django.db.models import Sum
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from operation.models import OperationModel
from operation.serializers import OperationStatsInSerializer


class OperationStatsView(APIView):

    def get(self, request: Request):
        serializer = OperationStatsInSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        user = request.user

        in_ops = user.operations.filter(state=OperationModel.State.SUCCESS, type=OperationModel.Type.INCOME)
        out_ops = user.operations.filter(state=OperationModel.State.SUCCESS, type=OperationModel.Type.OUTCOME)

        queued_count = user.operations.filter(state=OperationModel.State.QUEUED).count()
        processing_count = user.operations.filter(state=OperationModel.State.PROCESSING).count()
        success_count = user.operations.filter(state=OperationModel.State.SUCCESS).count()
        error_count = user.operations.filter(state=OperationModel.State.ERROR).count()

        income_count = in_ops.count()
        outcome_count = in_ops.count()
        income_amount = in_ops.aggregate(amount_sum=Sum('amount'))
        income_amount = income_amount['amount_sum'] or 0
        outcome_amount = out_ops.aggregate(amount_sum=Sum('amount'))
        outcome_amount = outcome_amount['amount_sum'] or 0
        operation_count = income_count + outcome_count

        return Response(dict(
            income_count=income_count, income_amount=income_amount,
            outcome_count=outcome_count, outcome_amount=outcome_amount,
            operation_count=operation_count, queued_count=queued_count,
            processing_count=processing_count, success_count=success_count,
            error_count=error_count))
