from rest_framework.generics import ListAPIView
from rest_framework.pagination import CursorPagination

from operation.models import OperationModel
from operation.serializers import OperationListInSerializer, OperationOutSerializer


class MarkerPagination(CursorPagination):

    ordering = '-created_at'
    page_size = 100


class OperationListView(ListAPIView):

    pagination_class = MarkerPagination
    serializer_class = OperationOutSerializer

    def get_queryset(self):
        serializer = OperationListInSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        datetime_from = self.request.query_params['datetime_from']
        datetime_to = self.request.query_params['datetime_to']

        return OperationModel.objects.filter(
            user=self.request.user,
            created_at__gte=datetime_from,
            created_at__lte=datetime_to)
