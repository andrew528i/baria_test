from django.urls import path

from operation.views.list import OperationListView
from operation.views.stats import OperationStatsView

urlpatterns = [
    path('', OperationListView.as_view()),
    path('stats', OperationStatsView.as_view()),
]
