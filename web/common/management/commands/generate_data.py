import random

from django.core.management import BaseCommand
from django.db import transaction
from tqdm import tqdm

from common.logging import logger
from operation.models import OperationModel
from user.models import UserModel


class Command(BaseCommand):

    help = 'Generate users & operations'

    def add_arguments(self, parser):
        parser.add_argument('--user_count', type=int, default=20000)
        parser.add_argument('--user_operation_count_from', type=int, default=5)
        parser.add_argument('--user_operation_count_to', type=int, default=10)
        parser.add_argument('--batch_size', type=int, default=5)

    def handle(self, *args, **options):
        user_count = options['user_count']
        user_created_count = 0
        user_operation_count_from = options['user_operation_count_from']
        user_operation_count_to = options['user_operation_count_to']
        batch_size = options['batch_size']

        logger.info('generating {} new users', user_count)

        with tqdm(total=user_count) as pbar:
            while user_created_count < user_count:
                users_to_create = min(batch_size, user_count - user_created_count)

                with transaction.atomic():
                    for _ in range(users_to_create):
                        user = UserModel.generate()
                        operation_count = random.randint(user_operation_count_from, user_operation_count_to)

                        for _ in range(operation_count):
                            OperationModel.generate(user)

                user_created_count += users_to_create
                pbar.update(users_to_create)

        logger.success('all done, new users: {}', user_created_count)
