import sys

import loguru


def stdout_filter(record):
    return record['level'].no <= 30


def stderr_filter(record):
    return record['level'].no > 30


def format_log(record):
    level_name = record['level'].name

    msg_color = 'w'

    if level_name == 'DEBUG':
        msg_color = 'light-blue'

    if level_name == 'INFO':
        msg_color = 'lw'

    if level_name == 'SUCCESS':
        msg_color = 'lg'

    if level_name == 'WARNING':
        msg_color = 'ly'

    if level_name in ('ERROR', 'CRITICAL'):
        msg_color = 'lr'

    fmt = (
        '<c>{time:YYYY-MM-DD} {time:HH:mm:ss}</c> '
        '{level.icon} '
        f' <{msg_color}>{{message}}</{msg_color}>\n')

    return fmt


def init_logger() -> loguru.logger:
    loguru_logger = loguru.logger.bind(name='tradelab')
    loguru_logger.configure(handlers=[])

    loguru_logger.add(sys.stdout, colorize=True, filter=stdout_filter, format=format_log)
    loguru_logger.add(sys.stderr, colorize=True, filter=stderr_filter, format=format_log)

    return loguru_logger


logger = None

if not logger:
    logger = init_logger()
