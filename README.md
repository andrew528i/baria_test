# Test task for Baria

# 👾 Run

```bash
cd docker
docker-compose up --build -d
```

---

## 💿 Generate users & operations

Iterating by batches needed amount of users to create and creating operations at every step of this cycle. One batch = one tx.

Class: `common.management.commands.generate_data.Command`

```bash
docker-compose run --rm web generate_data --user_count=20000 --user_operation_count_from=20 --user_operation_count_to=40
```

![generate_data](https://gitlab.com/andrew528i/baria_test/-/raw/dev/docs/generate_data.png)
![generate_data](https://gitlab.com/andrew528i/baria_test/-/raw/dev/docs/generate_data_result.png)

---

## ✅ Run tests

```bash
docker-compose run --rm web test
```

![generate_data](https://gitlab.com/andrew528i/baria_test/-/raw/dev/docs/test.png)

---

## 🍉 Most used api methods

### Operation list
Path: `GET /operation/?datetime_from=2020-08-10T00:00:00&datetime_to=2021-08-11T00:00`

Class: `operation.views.list.OperationListView`

Example response:
```json
{
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "amount": 378725,
      "symbol": "USD",
      "type": "IN",
      "state": "ERROR",
      "created_at": "1603993129",
      "updated_at": "1628617129"
    }
  ]
}
```

### Operation stats
Path: `GET /operation/stats/?datetime_from=2020-08-10T00:00:00&datetime_to=2021-08-11T00:00`

Class: `operation.views.stats.OperationStatsView`

Example response:
```json
{
  "income_count": 2,
  "income_amount": 102590,
  "outcome_count": 2,
  "outcome_amount": 3240305,
  "operation_count": 4,
  "queued_count": 10,
  "processing_count": 12,
  "success_count": 15,
  "error_count": 13
}
```

---

## 🎯 TODO
 - [x] Operation list API method
 - [ ] Use postgres as primary database
 - [ ] Create AssetModel to store fiat/crypto assets with decimals
 - [ ] Increase test coverage & quality
 - [ ] Set up CI
 - [ ] API methods for sign_up/sign_out
 - [ ] Benchmark execution time of most loaded API methods in CI
